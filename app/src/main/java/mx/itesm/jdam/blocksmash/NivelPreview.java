package mx.itesm.jdam.blocksmash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*
En esta actividad se despliega la información relacionada al niverl que se va a jugar.
*/

public class NivelPreview extends AppCompatActivity {

    private int nivel;
    private Button continuarBtn;
    private TextView nivelTitulo;
    private TextView reglasText;

    //Se declaran las variables como textos y botones y
    // se llama al método que decide que reglas se desplegarán.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivel_preview);

        String myStrings = getIntent().getStringExtra("nivel");
        nivel = Integer.parseInt(myStrings);
        if(nivel > 8) nivel = 8;
        continuarBtn = findViewById(R.id.coninueBtn);
        reglasText = findViewById(R.id.reglasText);

        nivelTitulo = findViewById(R.id.nivelTitulo);
        nivelTitulo.setText("Nivel " + nivel);
        setReglas(nivel);
    }

    //Método donde se escriben las condiciones para ganar el  nivel.
    public void setReglas(int nivel)
    {
        //Se busca qué nivel seleccionó el jugador y se eligen las reglas correspondientes..
        switch (nivel)
        {
            case 1:
                reglasText.setText("Junta 6000 puntos");
                break;
            case 2:
                reglasText.setText("Rompe 30 bloques morados");
                break;
            case 3:
                reglasText.setText("Rompe 40 bloques morados en 20 movimientos o menos");
                break;
            case 4:
                reglasText.setText("Que el bloque naranja llegue al fondo en menos de 6 movimientos");
                break;
            case 5:
                reglasText.setText("Consigue 3000 antes de que se acabe el tiempo");
                break;
            case 6:
                reglasText.setText("Que el bloque naranja llegue al fondo antes de que se acabe el tiempo");
                break;
            case 7:
                reglasText.setText("Rompe 30 bloques rojos antes de que se acabe el tiempo");
                break;
            case 8:
                reglasText.setText("Consigue 8000 sin romper más de 25 bloques verdes");
                break;
            case 9:
                reglasText.setText("Consigue 8000 sin romper más de 25 bloques verdes");
                break;
        }
    }

    //Método para cambiar a la actividad que contienen el nivel.
    public void cambiarNivel(View v)
    {

        Intent intent = new Intent(this, SmashActivity.class);
        intent.putExtra("nivel", "" + nivel);
        startActivity(intent);
        finish();
    }

    //Método para regresar a la actividad que contiene el menú principal.
    public void regresarMenu(View view)
    {
        Intent intent = new Intent(this, LevelSelection.class);
        startActivity(intent);
        finish();
    }
}
