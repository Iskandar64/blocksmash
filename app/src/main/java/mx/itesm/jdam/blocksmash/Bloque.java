package mx.itesm.jdam.blocksmash;

import android.util.Log;

//clase que representa a cada bloque dentro del nivel y su comportamiento
public class Bloque
{
    private int x;
    private int y;
    private int tipo;
    private Bloque derecha, arriba, abajo, izquierda;


    //Método cosntructor para definir los parámetros del bloque.
    public Bloque(int x, int y, Bloque arriba, Bloque abajo, Bloque derecha,
                  Bloque izquierda, int tipo)
    {
        //x define en qué columna se ubica el bloque
        this.x = x;
        //y define en qué fila se ubica el bloque
        this.y = y;
        //Las siguientes 4 variables determinan los bloques que están pegados al bloque.
        this.abajo = abajo;
        this.arriba = arriba;
        this.izquierda = izquierda;
        this.derecha = derecha;
        //tipo es una variable que determina el color de los bloques o sí es un bloque roto o especial.
        this.tipo = tipo;
    }



    //Método para buscar cuaántos bloques a la derecha tienen el mismo color
    public int buscarDerecha(int tipoDeseado)
    {
        if(derecha != null)
        {
            if(derecha.getTipo() == tipoDeseado)
            {
                return derecha.buscarDerecha(tipoDeseado) + 1;
            }
            else
            {
                return 0;
            }
        }
        return 0;
    }

    //Método para buscar cuaántos bloques a la izquierda tienen el mismo color
    public int buscarIzquierda(int tipoDeseado)
    {
        if(izquierda != null)
        {
            if(izquierda.getTipo() == tipoDeseado)
            {
                return izquierda.buscarIzquierda(tipoDeseado) + 1;
            }
            else
            {
                return 0;
            }
        }
        return 0;
    }

    //Método para buscar cuaántos bloques de arriba tienen el mismo color
    public int buscarArriba(int tipoDeseado)
    {
        if(arriba != null)
        {
            if(arriba.getTipo() == tipoDeseado)
            {
                return arriba.buscarArriba(tipoDeseado) + 1;
            }
            else
            {
                return 0;
            }
        }
        return 0;
    }

    //Método para buscar cuaántos bloques de abajo tienen el mismo color
    public int buscarAbajo(int tipoDeseado)
    {
        if(abajo != null)
        {
            if(abajo.getTipo() == tipoDeseado)
            {
                return abajo.buscarAbajo(tipoDeseado) + 1;
            }
            else
            {
                return 0;
            }
        }
        return 0;
    }



    //REgresa la variable ganador a su estado falso de todos los bloques a la derecha.
    public void liberarDerecha()
    {
        if(derecha != null)
        {
            derecha.liberarDerecha();
        }
    }



    //Metodo para obtener el número del bloque a partir de su posición en x y y.
    public int getPosicion() {
        return x + y*5;
    }

    //Metodos get y set para obtener información de los bloques
    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Bloque getDerecha() {
        return derecha;
    }

    public void setDerecha(Bloque derecha) {
        this.derecha = derecha;
    }

    public Bloque getArriba() {
        return arriba;
    }

    public void setArriba(Bloque arriba) {
        this.arriba = arriba;
    }

    public Bloque getAbajo() {
        return abajo;
    }

    public void setAbajo(Bloque abajo) {
        this.abajo = abajo;
    }

    public Bloque getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(Bloque izquierda) {
        this.izquierda = izquierda;
    }
}
