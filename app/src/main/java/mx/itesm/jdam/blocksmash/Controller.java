package mx.itesm.jdam.blocksmash;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

//Clase para controlar el comportamiento de los bloques como conjunto.
public class Controller {
    private ArrayList<Bloque> bloques;
    private int rojoCount;
    private int azulCount;
    private int verdeCount;
    private int moradoCount;
    private int movimeintosCount;

    //Método para inicializar las variables.
    public Controller(ArrayList<Integer> valores)
    {
        //Un arreglo que contiene a los bloques
        bloques = new ArrayList<>();
        //Método para llenar la lista de bloques
        crearBloques(valores);
        //Contadores de cuantos bloques de cada color se han roto.
        rojoCount = 0;
        azulCount = 0;
        verdeCount = 0;
        moradoCount = 0;
        movimeintosCount = 0;
    }

    //Método que determina si dos bloques están directamente conectados uno con el otro.
    public boolean sonVecinos(Bloque b1, Bloque b2)
    {
        if(b1.getAbajo() == b2)
        {
            return true;
        }
        else if(b1.getArriba() == b2)
        {
            return true;
        }
        else if(b1.getIzquierda() == b2)
        {
            return true;
        }
        else if(b1.getDerecha() == b2)
        {
            return  true;
        }
        else
        {
            return false;
        }

    }

    //Método para reiniciar los contadores y llenar la lista de bloques con nuevos.
    public void reiniciar(ArrayList<Integer> valores)
    {
        rojoCount = 0;
        azulCount = 0;
        verdeCount = 0;
        moradoCount = 0;
        movimeintosCount = 0;

        bloques.clear();
        crearBloques(valores);
    }


    //Método que genera nuevos valores (tipos) para los bloques que se rempieron.
    public ArrayList<Integer> generarRemplazos(int numero)
    {
        ArrayList<Integer> nuevos = new ArrayList<>();
        for(int i = 0; i < numero; i++)
        {
            int valor = new Random().nextInt(4);
            nuevos.add(valor);
        }
        return nuevos;
    }

    //Método para encontrar los bloques que se reompeiron (blancos) durante la
    //última jugada realizada.
    public int encontrarBlancos(int limite)
    {
        int count = 0;
        for(int i = 0; i < 6; i++)
        {
            if(bloques.get(limite - (5 * i)).getTipo() == 4)
            {
                count++;
            }
        }
        return count;
    }


    //Encuentra los bloques que no fueron rotos en la útlima jugada
    public ArrayList<Integer> encontrarNoBlancos(int limite)
    {
        ArrayList<Integer> returner = new ArrayList<>();
        for(int i = 0; i < 6; i++)
        {
            int index = limite - (5 * i);
            int tipo = bloques.get(index).getTipo();
            if(tipo != 4 && tipo != 5)
            {
                returner.add(tipo);
            }
        }
        return returner;
    }

    public void naranja(int posicion)
    {
        bloques.get(posicion).setTipo(6);
    }

    //Método que revisa si el bloque especial está al fondo del tablero
    public boolean checarFondo(int tipo)
    {
        Bloque bloque = bloques.get(25);
        for(int i = 25; i < 30; i++)
        {
            if(bloque.getTipo() == tipo)
            {
                return true;
            }
            bloque = bloque.getDerecha();
        }
        return false;
    }

    //Métodos que devuelven el número de bloques rotos dependiendo de su color.
    public int getRojoCount() {
        return rojoCount;
    }

    public int getAzulCount() {
        return azulCount;
    }

    public int getVerdeCount() {
        return verdeCount;
    }

    public int getMoradoCount() {
        return moradoCount;
    }

    //Método que devuelve el número de movimientos realizados.
    public int getMovimeintosCount() {
        return movimeintosCount;
    }

    /*
    LLeva a cabo la habilidad ColorActivity. Con esta, se destruyen todos los bloques del mismo
    color en el nivel.
     */
    public ArrayList<Integer> colorAbility(int color) {
        ArrayList<Integer> mismoColor = new ArrayList<>();
        //busca bloques del mismo color.
        for(Bloque bloque: bloques)
        {
            if(bloque.getTipo() == color)
            {
                mismoColor.add(bloque.getPosicion());
                actualizarCounters(bloque.getTipo());
                //Los cambia a color blanco, que significa que los bloques se rompieron.
                bloque.setTipo(4);
            }
        }
        return mismoColor;
    }

    /*
    LLeva a cabo la habilidad ColorActivity. Con esta, se destruyen todos los bloques de la
    misma columna correspondiente al bloque seleccionado.
     */
    public int columnaAbility(int position)
    {
        //Busca el primer bloque de la columna
        int blqPos = position % 5;
        Bloque bloque = bloques.get(blqPos);
        actualizarCounters(bloque.getTipo());
        bloque.setTipo(4);

        //Cambia el valor de todos los bloques a 4, o sea blanco.
        while (bloque.getAbajo() != null) {
            bloque = bloque.getAbajo();
            actualizarCounters(bloque.getTipo());
            bloque.setTipo(4);
        }
        return blqPos;
    }

    //Método que busca en todas las filas si hay 3 o más bloques del mismo color.
    public ArrayList<Integer> buscarHorizontal()
    {
        ArrayList<Integer> ganadores = new ArrayList<>();
        for(int i = 0; i < 6; i++)
        {
            int index = i * 5;

            Bloque bloque1 = bloques.get(index);
            int derecha1 = bloque1.buscarDerecha(bloque1.getTipo());
            Bloque bloque2 = bloques.get(index + 1);
            int derecha2 = bloque2.buscarDerecha(bloque2.getTipo());
            Bloque bloque3 = bloques.get(index + 2);
            int derecha3 = bloque3.buscarDerecha(bloque3.getTipo());

            if(derecha1 > 1 )
            {
                for (int k = index; k < index + derecha1 + 1; k++)
                {
                    ganadores.add(k);
                }
            }
            else if(derecha2 > 1)
            {
                for (int k = index + 1; k < index + derecha2 + 2; k++)
                {
                    ganadores.add(k);
                }
            }
            else if(derecha3 > 1)
            {
                ganadores.add(index + 2);
                ganadores.add(index + 3);
                ganadores.add(index + 4);
            }
        }
        return ganadores;
    }

    //Método que busca en todas las columnas si hay 3 o más bloques del mismo color.
    public ArrayList<Integer> buscarVertical()
    {
        ArrayList<Integer> ganadores = new ArrayList<>();

        for(int i = 0; i < 5; i++)
        {
            int index = i;

            Bloque bloque1 = bloques.get(index);
            int winners1 = bloque1.buscarAbajo(bloque1.getTipo()) + 1;
            for(int k = 1; k < 5; k++)
            {
                if(winners1 < 3)
                {
                    index = index + 5;
                    bloque1 = bloques.get(index);
                    winners1 = bloque1.buscarAbajo(bloque1.getTipo()) + 1;
                }
                else {
                    break;
                }
            }

            if(winners1 > 2)
            {

                for(int j = index; j < (index + 5 * winners1); j = j + 5)
                {
                    ganadores.add(j);
                }
            }
        }
        return ganadores;
    }

    //Método que busca si hay 3 o más bloques seguidos (vertical u horizontalmente(
    //Que tienen el mismo color. Si los encuentra los cambia a tipo 4 (color blanco).
    public ArrayList<Integer> buscarSecuenciaGanadora() {
        ArrayList<Integer> ganadores = buscarHorizontal();
        for(Integer integer: buscarVertical())
        {
            ganadores.add(integer);
        }

        //El arraylist se pasa a set para eliminar bloques repetidos.
        Set<Integer> set = new HashSet<>(ganadores);
        ganadores.clear();
        ganadores.addAll(set);

        for(Integer integer: ganadores)
        {
            actualizarCounters(bloques.get(integer).getTipo());
            bloques.get(integer).setTipo(4);
        }

        return ganadores;
    }

    //Método que actuaiza los contadores dependiendo del color/tipo del bloque.
    public void actualizarCounters(int tipo)
    {
        if(tipo == 0)
        {
            rojoCount++;
        }
        if(tipo == 1)
        {
            azulCount++;
        }
        if(tipo == 2)
        {
            verdeCount++;
        }
        if (tipo == 3)
        {
            moradoCount++;
        }
    }

    public void ponerGrises(ArrayList<Integer> posiciones)
    {
        for(Integer integer: posiciones)
        {
            bloques.get(integer).setTipo(5);
        }
    }

    /*
    Método que reacomoda los bloques luego de que 3 o más bloques se hayan roto.
     */
    public ArrayList<Integer> reacomodar()
    {
        for(int i = 25; i < 30; i++)
        {
            int blancos = encontrarBlancos(i);
            ArrayList<Integer> nuevos = encontrarNoBlancos(i);
            for(Integer integer: generarRemplazos(blancos))
            {
                nuevos.add(integer);
            }

            int contador = 0;
            for(int k = 0; k < 6; k++ )
            {
                Bloque blq = bloques.get(i - k * 5);
                if(blq.getTipo() == 5)
                {
                    continue;
                }

                blq.setTipo(nuevos.get(contador));
                contador++;
            }
        }

        ArrayList<Integer> valores = new ArrayList<>();
        for(Bloque bloque: bloques)
        {
            valores.add(bloque.getTipo());
        }
        return valores;
    }

    //Checamos si el cambio de posición entre dos bloque va a generar una secuencia ganadora
    public boolean checarNuevaPosicion(int b1, int b2)
    {
        Bloque bq1 = bloques.get(b1);
        Bloque bq2 = bloques.get(b2);

        int verticalSumB1 = 0;
        int horizontalSumB1 = 0;
        int verticalSumB2 = 0;
        int horizontalSumB2 = 0;

        if(bq1.getAbajo() != bq2)
        {
            verticalSumB1 += bq1.buscarAbajo(bq2.getTipo());
            verticalSumB2 += bq2.buscarArriba(bq1.getTipo());
        }
        if(bq1.getArriba() != bq2)
        {
            verticalSumB1 += bq1.buscarArriba(bq2.getTipo());
            verticalSumB2 += bq2.buscarAbajo(bq1.getTipo());
        }
        if(bq1.getDerecha() != bq2)
        {
            horizontalSumB1 += bq1.buscarDerecha(bq2.getTipo());
            horizontalSumB2 += bq2.buscarIzquierda(bq1.getTipo());
        }
        if(bq1.getIzquierda() != bq2)
        {
            horizontalSumB1 += bq1.buscarIzquierda(bq2.getTipo());
            horizontalSumB2 += bq2.buscarDerecha(bq1.getTipo());
        }

        if((verticalSumB1 >= 2 || horizontalSumB1 >= 2) || (verticalSumB2 >= 2 || horizontalSumB2 >= 2))
        {
            return true;
        }
        else {
            return false;
        }
    }

    //Método que permite que dos bloques de colores distitntos intercambien su valor.
    //El contador de movimeintos aumenta cada véz que se llama este método.
    public boolean intercambiarBloques(int b1, int b2)
    {
        Bloque bq1 = bloques.get(b1);
        Bloque bq2 = bloques.get(b2);
        boolean checarViabilidad = checarNuevaPosicion(b1, b2);
        if(sonVecinos(bq1, bq2) && checarViabilidad)
        {
            int type = bq1.getTipo();
            bq1.setTipo(bq2.getTipo());
            bq2.setTipo(type);
            movimeintosCount++;
            return true;
        }
        return false;
    }

    //Kétodo que crea los bloques a partir de los valores recibidos.
    public void crearBloques(ArrayList<Integer> valores)
    {
        int count = 0;
        while (valores.size() > count)
        {
            int x = count % 5;
            int y = count / 5;
            Bloque blq = new Bloque(x, y, null, null, null, null,
                    valores.get(count));
            bloques.add(blq);
            count++;
        }
        relacionarBloques();
    }

    //Método usado para test donde se muestran los bloques que están arriba,
    //abajo, derecha e izquierda del bloque seleccionado
    public String mostrarVecinos(int posicion)
    {
        Bloque bloque = bloques.get(posicion);
        String vecinos = "";
        if(bloque.getArriba() != null){
            vecinos += "\t " + bloque.getArriba().getPosicion() + "(" + bloque.getArriba().getTipo() + ")\n";
        }
        else{
            vecinos += "\n";
        }
        if(bloque.getIzquierda() != null){
            vecinos += bloque.getIzquierda().getPosicion()+ "(" + bloque.getIzquierda().getTipo() + ")"+ "\t";
        }
        else {
            vecinos +=  "\t";
        }
        vecinos +=  bloque.getPosicion() +"(" +bloque.getTipo() + ")";
        if(bloque.getDerecha() != null){
            vecinos += "\t" + bloque.getDerecha().getPosicion()+ "(" + bloque.getDerecha().getTipo() + ")"+ "\n";
        }
        else {
            vecinos +=  "\n";
        }
        if(bloque.getAbajo() != null)
        {
            vecinos += "\t" + bloque.getAbajo().getPosicion()+"(" + bloque.getAbajo().getTipo() + ")";
        }
        else {
            vecinos += "\n";
        }
        return   vecinos;
    }

    /*
    Método que define los vecinos de cada bloque. Si un bloque están en los extremos sus vecinos
    correspondientes serán null. Por ejemplo, si un bloque está en el extremo derecho de la primera
    fila, entonces sus vecinos de arriba y derecha serán null.
     */
    public  void relacionarBloques()
    {
        int count = 0;
        for(Bloque blq: bloques)
        {
            if (blq.getX() > 0)
            {
                blq.setIzquierda(bloques.get(count - 1));
            }
            if(blq.getX() < 4)
            {
                blq.setDerecha(bloques.get(count + 1));
            }
            if(blq.getY() > 0)
            {
                blq.setArriba(bloques.get(count - 5));
            }
            if(blq.getY() < 5)
            {
                blq.setAbajo(bloques.get(count + 5));
            }
            count++;
        }
    }
}
