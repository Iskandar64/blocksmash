package mx.itesm.jdam.blocksmash;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import static mx.itesm.jdam.blocksmash.Colores.*;

public class SmashActivity extends AppCompatActivity {

    private ArrayList<Integer> valores, switchValores;
    private Controller controller;
    private ArrayList<Button> buttons;


    private int level;
    private int lastPressed;
    int size;

    private int score;
    private TextView marcador;
    private TextView nivelText;
    private TextView condicionText;
    private TextView timerText;

    private boolean colorAbility;
    private boolean filaAbilty;
    private int countColores;
    private int countFila;

    private Button filaButton;

    private int nivel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_smash);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        String myStrings = getIntent().getStringExtra("nivel");
        nivel = Integer.parseInt(myStrings);

        buttons = new ArrayList<>();
        switchValores = new ArrayList<>();
        size = 1;
        level = 1;

        lastPressed = 99;
        filaButton = findViewById(R.id.FilaAbility);

        score = 0;
        marcador = findViewById(R.id.Putnos);
        marcador.setText("Puntos: " + score);
        timerText = findViewById(R.id.timer);
        timerText.setVisibility(View.INVISIBLE);
        condicionText = findViewById(R.id.condicion);
        condicionText.setVisibility(View.INVISIBLE);

        valores = new ArrayList<>();
        generarColores();

        controller = new Controller(valores);
        colorearBloques();

        colorAbility = false;
        filaAbilty = false;

        countColores = 0;
        nivelText = findViewById(R.id.NivelDisplay);
        nivelText.setVisibility(View.VISIBLE);
        NivelTextManager();

        countFila = 0;
    }

    //Método para regresar al menú rpincipal.
    public void MenuPrincipal(View v)
    {
        Intent intent = new Intent(this, LevelSelection.class);
        startActivity(intent);
        finish();
    }


    //Método para reiniciar el nivel con los parámetros por default
    public void reiniciar()
    {
        valores.clear();
        buttons.clear();
        generarColores();
        colorearBloques();
        switchValores.clear();
        controller.reiniciar(valores);
        actualizarPuntuacion(-99);
        colorAbility = false;
        filaAbilty = false;
        countColores = 0;
        countFila = 0;
        NivelTextManager();
    }

    //Método que despliega las cajas de texto y su contenido para definir la misión del nivel
    public void NivelTextManager()
    {
        nivelText.setText("Nivel: " + String.valueOf(nivel));
        switch (nivel) {
            case 1:
                condicionText.setVisibility(View.INVISIBLE);
                break;
            case 2:
                condicionText.setVisibility(View.VISIBLE);
                condicionText.setText("Morados = 0/30");
                break;
            case 3:
                condicionText.setVisibility(View.VISIBLE);
                condicionText.setText("Morados = 0/40. \n Movimientos= 0/20");
                break;
            case 4:
                condicionText.setVisibility(View.VISIBLE);
                valores.set(0, 6);
                pintarBloque(0, 6);
                controller.naranja(0);
                filaButton.setVisibility(View.INVISIBLE);
                condicionText.setText(" Movimientos= 0/6");
                break;
            case 5:
                timerText.setVisibility(View.VISIBLE);
                setCountDown(40000);
                break;
            case 6:
                timerText.setVisibility(View.VISIBLE);
                valores.set(0, 6);
                pintarBloque(0, 6);
                controller.naranja(0);
                filaButton.setVisibility(View.INVISIBLE);
                setCountDown(40000);
                break;
            case 7:
                condicionText.setVisibility(View.VISIBLE);
                condicionText.setText("Rojos = 0/30");
                timerText.setVisibility(View.VISIBLE);
                setCountDown(60000);
                break;
            case 8:
                condicionText.setVisibility(View.VISIBLE);
                condicionText.setText("Verdes = 0/25");
                break;
            case 10:
                valores.set(8, 5);
                valores.set(9, 5);
                pintarBloque(8, 5);
                pintarBloque(9, 5);
                controller.ponerGrises(new ArrayList<Integer>( Arrays.asList(8, 9)));
                //nivelText.setText("Nivel 5: consigue 10000 antes de que se acabe el tiempo");
                condicionText.setVisibility(View.INVISIBLE);
                break;
        }
    }

    /*
    Método que modifica el contenido de las cajas de texto con el fin de actualizar
    el status de la sesión del juego y los requerimeintos para completar el nivel
     */
    public void NivelProgresionManager()
    {
        Log.i("Nivel", String.valueOf(nivel));
        switch (nivel)
        {
            case 1:
                if(score > 6000)
                {
                    cambioDeNivel(2);
                }
                break;
            case 2:
                int morados = controller.getMoradoCount();
                Log.i("Morados", String.valueOf(morados));
                condicionText.setText("Morados = " + morados + "/30");
                if(morados >= 30)
                {
                    cambioDeNivel(3);
                }
                break;
            case 3:
                int moradoCount = controller.getMoradoCount();
                int movimientos = controller.getMovimeintosCount();
                condicionText.setText("Morados = " + moradoCount + "/40. \nMovimientos= " + movimientos +"/20");
                if(movimientos > 30)
                {
                    reiniciar();
                    break;
                }

                if(moradoCount >= 30)
                {
                    cambioDeNivel(4);
                }
                break;
            case  4:
                movimientos = controller.getMovimeintosCount();
                condicionText.setText("Movimientos= " + movimientos +"/6");
                if(movimientos > 6)
                {
                    reiniciar();
                    break;
                }
                if(controller.checarFondo(6))
                {
                    cambioDeNivel(5);
                }

                break;
            case 5:
                if(score > 3000)
                {
                    cambioDeNivel(6);
                }
                break;
            case 6:
                if(controller.checarFondo(6))
                {
                    cambioDeNivel(7);
                }
                break;
            case 7:
                int rojos = controller.getRojoCount();
                condicionText.setText("Rojos = " + rojos + "/30");
                if(rojos >= 30)
                {
                    cambioDeNivel(8);
                }
                break;
            case 8:
                int verdes = controller.getVerdeCount();
                condicionText.setText("Verdes = " + verdes + "/25");
                if(verdes > 25)
                {
                    reiniciar();
                }
                else if(score > 8000)
                {
                    cambioDeNivel(1);
                }
                break;
        }
    }

    //Método activado al completar el nivel que envía al usuario al previo del sigueinte nivel
    private void cambioDeNivel(int nextLevel)
    {
        Intent intent = new Intent(this, NivelPreview.class);
        intent.putExtra("nivel", "" + nextLevel);
        startActivity(intent);
        finish();
    }

    //Método que inicia la cuenta regresiva para ciertos niveles. Además que actualiza el tiempo restante.
    private void setCountDown(int time)
    {
        new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
                timerText.setText("Segundos restantes " + millisUntilFinished / 1000 );
            }

            public void onFinish() {
                reiniciar();
                timerText.setText("done!");
            }
        }.start();
    }

    //Método que actualiza la puntuación.
    public void actualizarPuntuacion(int puntos)
    {
        if(puntos == -99)
        {
            score = 0;
        }
        else
        {
            score += puntos * 100;
        }
        marcador.setText("Puntos: " + score);
    }

    //Método que controla los colores del bloque después de que el jugador hace una jugada.
    public void intercambiarValores(View view)
    {
        String ID = (view.getResources().getResourceName(view.getId())).
                replace("mx.itesm.jdam.blocksmash:id/", "");
        String [] caracteres = ID.split("(?!^)");
        int x = Integer.parseInt(caracteres[2]);
        int y = Integer.parseInt(caracteres[1]) * 5;
        int posicion = y + x;

        int vv = valores.get(posicion);
        if(vv == 5)
        {
            return;
        }
        oscurecerBloque(posicion, vv);
        size = 1;
        if(filaAbilty)
        {
            if(vv != 6)
            {
                blanquearFila(posicion);
                pintarBloque(posicion, valores.get(posicion));
            }
            return;
        }
        else if(colorAbility)
        {
            if(vv != 6)
            {
                blanquearColores(posicion);
            }
            return;
        }

        Log.i("vecinos", controller.mostrarVecinos(posicion));

        if(posicion != lastPressed && vv != 4)
        {
            if(switchValores.isEmpty())
            {
                switchValores.add(posicion);
                lastPressed = posicion;
            }
            else {
                int previous = switchValores.get(0);

                if(controller.intercambiarBloques(posicion, previous))
                {
                    int temp = valores.get(posicion);
                    valores.set(posicion, valores.get(previous));
                    valores.set(previous, temp);

                    pintarBloque(previous, valores.get(previous));
                    pintarBloque(posicion, valores.get(posicion));

                    buscarGanadores();
                }
                else
                {
                    pintarBloque(previous, valores.get(previous));
                    pintarBloque(posicion, valores.get(posicion));
                }
                lastPressed = 99;
                switchValores.clear();
            }
        }
        else
        {
            pintarBloque(posicion, valores.get(posicion));
            lastPressed = 99;
            switchValores.clear();
        }
    }

    //Méodo que lleva a cabo el color activity donde los bloques del mismo color al
    //seleccionado son elimnados
    public void activarColorAbility(View view)
    {
        if(lastPressed != 99){pintarBloque(lastPressed, valores.get(lastPressed));}
        lastPressed = 99;
        switchValores.clear();

        filaAbilty = false;

        if(colorAbility)
        {
            colorAbility = false;
        }
        else if(score > 1000 && countColores < 2)
        {
            colorAbility = true;
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No tienes suficientes puntos", Toast.LENGTH_SHORT).show();
            /*AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
            dialogo.setTitle("Aviso").setMessage("No tienes suficientes puntos").setPositiveButton("Aceptar",
                    null);
            dialogo.show();*/
        }
    }

    //Método que blanquea los bloques que tengan el mismo color del bloque seleccionado.
    public void blanquearColores(int posicion)
    {
        countColores++;
        ArrayList<Integer> mismoColor = controller.colorAbility(valores.get(posicion));
        for (Integer integer: mismoColor)
        {
            valores.set(integer, 4);
            pintarBloque(integer, 4);
        }
        actualizarPuntuacion(-8);
        arreglar();
    }

    //Método que verifica que el usuario pueda usar COlumnAbility y procesa los
    //movimientos del usuairo para que no choque con otras funciones del juego
    public void activarColumnaAbility(View view)
    {
        if(lastPressed != 99){pintarBloque(lastPressed, valores.get(lastPressed));}
        lastPressed = 99;
        switchValores.clear();
        colorAbility = false;

        if(filaAbilty)
        {
            filaAbilty = false;
        }
        else if(score > 1500 && countFila < 2)
        {
            filaAbilty = true;
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No tienes suficientes puntos", Toast.LENGTH_SHORT).show();
        }
    }

    //Método que blanquea los bloques que estén en la misma columna que el bloque seleccionado.
    public void blanquearFila(int posicion)
    {
        int top = controller.columnaAbility(posicion);
        actualizarPuntuacion(-15);
        countFila++;

        while(top < 30)
        {
            if(valores.get(top) != 6)
            {
                valores.set(top, 4);
                pintarBloque(top, 4);
            }
            top = top + 5;
        }
        actualizarPuntuacion(5);


        arreglar();
    }

    /*
    Método que asigna los nuevos valores (colores) a los bloques.
     */
    public void arreglar()
    {
        filaAbilty = false;
        colorAbility = false;
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                ArrayList<Integer> values = controller.reacomodar();
                cambiarColores(values);
                buscarGanadores();
                lastPressed = 99;
                switchValores.clear();

            }
        }, 250);
    }

    /*Método que obtiene las secuencias ganadoras en el nivel y actualiza el status de la
    sesión de juego. También pinta de blanco a los bloques con secuencia ganadora.
     Tiene un retraso de 0.450 para que el usuaro vea como se pintan de
    blanco los bloques que se "rompieron".*/
    public int buscarGanadores()
    {
        Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            public void run() {
                ArrayList<Integer> enteros = controller.buscarSecuenciaGanadora();
                size = enteros.size();
                for(Integer integer: enteros)
                {
                    valores.set(integer, 4);
                    pintarBloque(integer, 4);
                }
                if(enteros.size() > 0)
                {
                    actualizarPuntuacion(enteros.size());
                    arreglar();
                }
                else
                {
                    NivelProgresionManager();
                }

            }
        }, 450);
        return size;
    }

    //Intercambia los colores (tipos) de dos bloques
    public void cambiarColores(ArrayList<Integer> values)
    {
        valores.clear();
        for(int i =0; i < 30; i++)
        {
            valores.add(values.get(i));
            pintarBloque(i, values.get(i));
        }
    }

    //Método que cambia el color de un bloque seleccionado por un más oscuro.
    public void oscurecerBloque(int index, int tipo)
    {
        String color = " ";
        switch (tipo)
        {
            case 0:
                color = darkRed;
                break;
            case 1:
                color = darkBlue;
                break;
            case 2:
                color = darkGreen;
                break;
            case 3:
                color = darkPurple;
                break;
            case 4:
                color = white;
                break;
            case 6:
                color = darkOrange;
                break;
        }
        buttons.get(index).setBackgroundColor(Color.parseColor(color));
    }

    //Método que pinta un bloque con el color enviado
    public void pintarBloque(int index, int tipo)
    {
        String color = " ";
        switch (tipo)
        {
            case 0:
                color = red;
                break;
            case 1:
                color = blue;
                break;
            case 2:
                color = green;
                break;
            case 3:
                color = purple;
                break;
            case 4:
                color = white;
                break;
            case 5:
                color = gray;
                break;
            case 6:
                color = orange;
                break;
        }
        buttons.get(index).setBackgroundColor(Color.parseColor(color));
    }

    //Método que pinta todos los bloques a partir de los valores calcualdos aleatoriamente.
    public void colorearBloques()
    {
        for(int i = 0; i < valores.size(); i++)
        {
            int x = i % 5;
            int y = i / 5;

            String buttonID = "B" + y + x;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            Button btn = ((Button) findViewById(resID));
            buttons.add(btn);
            pintarBloque(i, valores.get(i));
        }
    }

    //Método que asigna valores a los bloques. los valores (tipos) son generado aleatoriamente
    //de 4 tipos para elegir.
    public void generarColores() {
        //generamos valores aleatorios
        for (int i = 0; i < 30; i++) {
            int valor = new Random().nextInt(4);
            valores.add(valor);
        }

        //vemos que no ahya una combianción ganadora en la primera fila
        int tempValor = valores.get(2);
        while (tempValor == valores.get(1) || tempValor == valores.get(3)) {
            tempValor = new Random().nextInt(4);
        }
        valores.set(2, tempValor);


        //vemos que no haya una combianción ganadora en la segunda fila y en ninguna columna
        int up;
        for (int i = 5; i < 10; i++) {
            int change = 1;
            up = valores.get(i - 5);
            if (up == 3) {
                change = -1;

            }
            valores.set(i, up + change);
        }

        tempValor = valores.get(7);
        up = valores.get(2);
        while (tempValor == valores.get(6) || tempValor == valores.get(8) || tempValor == up) {
            tempValor = new Random().nextInt(4);
        }
        valores.set(7, tempValor);


        //vemos que no haya una combianción ganadora en la tercera fila
        tempValor = valores.get(12);
        while (tempValor == valores.get(11) || tempValor == valores.get(13)) {
            tempValor = new Random().nextInt(4);
        }
        valores.set(12, tempValor);


        //vemos que no haya una combianción ganadora en la cuarta fila y en ninguna columna
        for (int i = 15; i < 20; i++) {
            int change = 1;
            up = valores.get(i - 5);
            if (up == 3) {
                change = -1;

            }
            valores.set(i, up + change);
        }

        tempValor = valores.get(17);
        up = valores.get(12);
        while (tempValor == valores.get(16) || tempValor == valores.get(18) || tempValor == up) {
            tempValor = new Random().nextInt(4);
        }
        valores.set(17, tempValor);


        //vemos que no haya una combianción ganadora en la quinta fila y
        tempValor = valores.get(22);
        while (tempValor == valores.get(21) || tempValor == valores.get(23)) {
            tempValor = new Random().nextInt(4);
        }
        valores.set(22, tempValor);


        //vemos que no haya una combianción ganadora en la sexta fila y en ninguna columna
        for (int i = 25; i < 30; i++) {
            int change = 1;
            up = valores.get(i - 5);
            if (up == 3) {
                change = -1;

            }
            valores.set(i, up + change);
        }

        tempValor = valores.get(27);
        up = valores.get(22);
        while (tempValor == valores.get(26) || tempValor == valores.get(28) || tempValor == up) {
            tempValor = new Random().nextInt(4);
        }
        valores.set(27, tempValor);
    }
}
