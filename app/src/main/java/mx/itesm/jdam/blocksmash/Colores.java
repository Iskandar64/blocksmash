package mx.itesm.jdam.blocksmash;

public final class Colores {
    public static final String gray = "#808080";
    public static final String darkGray = "#696969";
    public static final String red = "#FF0000";
    public static final String darkRed = "#8B0000";
    public static final String green = "#00FF00";
    public static final String darkGreen = "#006400";;
    public static final String blue = "#00CED1";
    public static final String darkBlue = "#00008B";
    public static final String purple = "#9932CC";
    public static final String darkPurple = "#800080";
    public static final String orange = "#FFA500";
    public static final String darkOrange = "#FF4500";
    public static final String white = "#FFFFFF";
}
