package mx.itesm.jdam.blocksmash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/*
Menú Principal donde se despliegan los niveles que el jugador puede jugar, 8 de momento.
*/
public class LevelSelection extends AppCompatActivity {

    //Método que despleiga el layout de la actividad.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_selection);
    }

    //Método que manda al usuario a la actividad que contiene
    //el preview del nivel que eligió. Se activa al presionar un botón
    public void mandarANivel(View view)
    {
        //Se lee el ID del botón para determinar a que nivel quiere ir
        String ID = (view.getResources().getResourceName(view.getId())).
                replace("mx.itesm.jdam.blocksmash:id/", "");
        String [] caracteres = ID.split("-");
        String nivelNumero = caracteres[1];

        //manda al usuario al previo y envía a la actividad qué nivel eligió el usuario
        Intent intent = new Intent(this, NivelPreview.class);
        intent.putExtra("nivel", "" + nivelNumero);
        startActivity(intent);
    }
}


